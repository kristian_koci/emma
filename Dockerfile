FROM node:10
WORKDIR /emma
COPY package.json /emma
RUN npm install
COPY . /emma
CMD node index.js
EXPOSE 3001
